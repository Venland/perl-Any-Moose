Name:           perl-Any-Moose
Summary:        Use Moose or Mouse automagically (DEPRECATED)
Version:        0.27
Release:        1
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/dist/Any-Moose
Source0:        https://cpan.metacpan.org/authors/id/E/ET/ETHER/Any-Moose-%{version}.tar.gz
BuildArch:      noarch
# Build
BuildRequires:  coreutils
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Moose)
# Runtime
BuildRequires:  perl(Carp)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Test Suite
BuildRequires:  perl(CPAN::Meta) >= 2.120900
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(Mouse) >= 0.40
BuildRequires:  perl(Test::More) >= 0.88
%if !0%{?perl_bootstrap}
BuildRequires:  perl(MooseX::Types)
BuildRequires:  perl(MouseX::Types)
%endif
# Dependencies
# Virtual provides in perl-Moose and perl-Mouse
Requires:       perl(Any-Moose) >= 0.40
Requires:       perl(Carp)

%description
Any::Moose is deprecated - please use Moo for new code.

This module allows one to take advantage of the features Moose/Mouse
provides, while allowing one to let the program author determine if Moose
or Mouse should be used; when use'd, we load Mouse if Moose isn't already
loaded, otherwise we go with Moose.

%prep
%setup -q -n Any-Moose-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%doc Changes CONTRIBUTING README
%{perl_vendorlib}/Any/

%changelog
* Wed Jul 05 2023 leeffo <liweiganga@uniontech.com> - 0.27-1
- init package
